import './sass/main.scss';
import { search } from './scripts/form';
import { tooltipMenu, tooltipShow } from './scripts/menu';
tooltipShow();
tooltipMenu();
search();
