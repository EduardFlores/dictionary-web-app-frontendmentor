import { requestApi } from './request';

const $inputSearch = document.getElementById('input-search');
const $iconSearch = document.getElementById('icon-search');
const $errorSearch = document.getElementById('error-search');
const classManager = () => {
  if ($inputSearch.value === '') {
    $errorSearch.classList.add('error-input-search-show');
    $inputSearch.style.outline = '1px solid red';
  } else {
    $errorSearch.classList.remove('error-input-search-show');
    requestApi($inputSearch.value);
  }
};
const eventManager = {
  handleEvent: (event) => {
    switch (event.type) {
      case 'click':
        classManager();
        break;
      case 'input':
        if (event.target.value !== '') {
          $errorSearch.classList.remove('error-input-search-show');
          $inputSearch.style.outline = '1px solid var(--color-primary)';
        }
        break;
      case 'keyup':
        if (event.keyCode === 13) {
          classManager();
        }
        break;
    }
  },
};
const searchEvents = () => {
  $iconSearch.addEventListener('click', eventManager);

  $inputSearch.addEventListener('input', eventManager);

  $inputSearch.addEventListener('keyup', eventManager);
};
export { searchEvents as search };
