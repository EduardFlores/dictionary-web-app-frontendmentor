// const requestApi = async (query) => {
//   const getApi = await fetch(
//     `https://api.dictionaryapi.dev/api/v2/entries/en/${query}`
//   );
//   console.log(getApi);
//   const response = await getApi.json();
// };
// requestApi('key');
const $templateResult = document.getElementById('template-result').content;

const fragment = document.createDocumentFragment();

const requestApi = async (query) => {
  return fetch(`https://api.dictionaryapi.dev/api/v2/entries/en/${query}`)
    .then((response) => response.json())
    .then((data) => createElements(data));
};

const createElements = (response) => {
  const result = response[0];

  $templateResult.getElementById('word').textContent = result.word;
  uploadElements();
};

const uploadElements = () => {
  const clone = $templateResult.cloneNode(true);
  fragment.append(clone);
  cleanDom(document.querySelector('main'));
  document.querySelector('main').append(fragment);
};

const cleanDom = (element) => {
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
};
// cambiar el nombre de la variable result
export { requestApi };
