const $buttonMenu = document.querySelector('.btn-menu');
const $tooltip = document.querySelector('.tooltip');
const $body = document.querySelector('body');

const tooltipShow = () => {
  $buttonMenu.addEventListener('click', () => {
    $tooltip.classList.toggle('tooltip-show');
  });
};

const tooltipMenu = () => {
  $tooltip.addEventListener('click', (event) => {
    const font = event.target.dataset.font;
    $body.classList.toggle(font);
    document.body.classList.remove('sans-serif', 'serif', 'mono');
    document.body.classList.add(font);
    $buttonMenu.textContent = font.replace('-', ' ');
  });
};

export { tooltipShow, tooltipMenu };
